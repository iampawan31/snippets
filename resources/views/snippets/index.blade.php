@component('layout')
    @if (count('snippets'))
        @foreach ($snippets as $snippet)
            <article class="snippet">
            <div class="is-flex">
                <h4 class="title flex">
                    <a href="/snippets/{{ $snippet->id }}">
                        {{ $snippet->title }}
                    </a>
                </h4>
                <a href="/snippets/{{ $snippet->id }}/fork" class="button is-primary">Fork Me</a>
            </div>    
                <pre>
                    <code>
                        {{ $snippet->body }}
                    </code>
                </pre>
            </article>
        @endforeach
    @endif
@endcomponent